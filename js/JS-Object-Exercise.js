"use strict"
//
// TODO: done
// * Create an object with firstName and lastName properties that are strings
// * with your first and last name. Store this object in a variable named
// * `person`.
// *
// * Example:
// *  > console.log(person.firstName) // "Tim"
// *  > console.log(person.lastName) // "Duncan"


//  ******

// var person ={
//  firstName = 'Sandra',
//  lastName = 'Linares',
//
// };
// console.log(person.firstName);
// console.log(person.lastName);

// */
// /**
//  * TODO: done
//  * Add a sayHello method to the person object that returns a greeting using
//  * the firstName and lastName properties.
//  * console.log the returned message to check your work
//  *
//  * Example
//  * > console.log(person.sayHello()) // "Hello from Tim Duncan!"
//  */
// ********
//
// var person = {
//  firstName = 'Sandra',
//  lastName = 'Linares',
//  greet = function(){
// },
//  sayHello: function(){
//   return 'Hello from' + this.firstName +' ' + this.lastName;
// }
// };
//   console.log(person.sayHello());




/** TODO: done
 *
 * HEB has an offer for the shoppers that buy products amounting to
 * more than $200. If a shopper spends more than $200, they get a 12%
 * discount. Write a JS program, using conditionals, that logs to the
 * browser, how much Karen, Pibo and Juan need to pay. We know that
 * Pibo bought $180, Karen $250 and Juan $320. Your program will have to
 * display a line with the name of the person, the amount before the
 * discount, the discount, if any, and the amount after the discount.
 *
 * Use a foreach loop to iterate through the array,
 * and console.log the relevant messages for each person
 *
 * Use the following array of objects
 */
//
//  var shoppers = [
//    {name: 'Pibo', amount: 180},
//    {name: 'Karen', amount: 250},
//    {name: 'Juan', amount: 320}
//  ];
//
// var discount = .12;
//
// shoppers.forEach(function (shopper) {
//     var output = '${shopper.name} bought $${shopper.amount}.';
//
//     if (shopper.amount >= 200){
//
//       output += '\nDiscount: ' + discount + '/nTotal: $ ' + (shopper.amount) * (1 - discount)
//
//     }
// console.log(output)
// })

//  *
//  *
//  */
/** TODO: done
 * Create an array of objects that represent books and store it in a
 * variable named `books`. Each object should have a title and an author
 * property. The author property should be an object with properties
 * `firstName` and `lastName`. Be creative and add at least 5 books to the
 * array
 */
/**
 * Example:
 * > console.log(books[0].title) // "IT"
 * > console.log(books[0].author.firstName) // "Stephen"
 * > console.log(books[0].author.lastName) // "King"
 */
//
// var books = [
//   {
//     title: 'IT',
//     author:{
//         firstName: 'Stephen',
//         lastName: 'King'
//     }
//
//   },
//   {
//     title: 'The Handmaid\'s Tale',
//     author:{
//       firstName: 'Margaret',
//       lastName: 'Atwood'
//   }
//   },
//   {    title: 'Fire and Blood',
//         author:{
//         firstName: 'George',
//         lastName: 'R.R. Martin'
//     }
// },
// {
//   title: 'Harry Potter',
//     author: {
//     firstName: 'J.K.',
//       lastName: 'Rowling'
//
//
//   }
// },
// {
//
//   title: 'Batman',
//     author: {
//     firstName: '',
//       lastName: 'King'
//   }
// },
//
//
// ]
//
// console.log(books[0].title) // 'IT'
// console.log(books[0].author.lastName) // 'Stephen'
// console.log(books[0].author.lastName)


/**
 * TODO: done
 * Loop through the books array and output the following information about
 * each book:
 * - the book number (use the index of the book in the array)
 * - the book title
 * - author's full name (first name + last name)
 */
/**
 * Example Console Output:
 *
 *      Book # 1
 *      Title: IT
 *      Author: Stephen King
 *      ---
 *      Book # 2
 *      Title: The Handmaid's Tale
 *      Author: Margaret Atwood
 *      ---
 *      Book # 3
 *      Title: Fire and Blood
 *      Author: George R.R. Martin
 *      ---
 *      ...
 */
// for(var i = 0; i < book.length; i++){
//   console.log(
//       'Book # ' + parseInt(i + 1) +
//       '\nTitle: ' + books[i].title +
//       '\nAuthor: ' + books[i].author.firstName + '' + books[i].author.lastName +
//       '\n--'
//   );
// }

 // */
/**
 * Bonus:
 * - Create a function named `createBook` that accepts a title and author
 *   name and returns a book object with the properties described
 *   previously.
 *   Refactor your code that creates the books array to instead
 *   use your function.
 * - Create a function named `showBookInfo` that accepts a book object and
 *   outputs the information described above. Refactor your loop to use your
 *   `showBookInfo` function.
 */
