"use strict";

var myRequest = new XMLHttpRequest();

myRequest.open('GET', 'https://swapi.dev/api/planets/1/');

myRequest.onreadystatechange = function () {

  if (myRequest.readyState === 4) {

    document.getElementById('api-content').innerHTML = myRequest.responseText;
  }
};
function sendTheAPI() {
  myRequest.send();
  document.getElementById('reveal').style.display = 'none';
}
