"use strict"
console.log('Hello from Javascript External');

//
// -Write some JavaScript that uses a 'confirm' dialog to ask the user if they would like to enter a number.
//   confirm ('would you like to enters a number?')
// - If they click 'Ok', prompt the user for a number, then use 3 separate alerts to tell the user:
// confirm js syntax   'Would you like to enter a number?
//  prompt('Enter a number')
//   - Whether the number is even
//  if(x % 2 === 0)
// - What the number plus 100 is
//   (even + 100)  alert
// - Whether the number is odd
//    (x % 2 !==0)
//   alert (odd + 100)
// - If the number is negative or positive
//    if (0 > userinput)
//    alert ('your number is negative')
//    alert (negative + 100)
//
//    if (0 < userinput)
//    alert ('your number is positive')
//    alert (postive + 100)
// If the user doesn't enter a number, use an alert to tell them that, and do not display any of the information above
//    alert ('maybe, some other time ')


//  ****  SOLUTION TO PROBLEM 1
//
// if (confirm('Would you like to enter a number?') == true) {
//   let userInput = prompt(' Enter a number ')
//   if (userInput % 2 === 0 && userInput > 0) {
//     alert(' you picked a positive even number ');
//     alert(' your number ' + userInput + ' plus 100 is ' + (Number(userInput) + 100))
//   }
//   else if (userInput % 2 !== 0 && userInput > 0) {
//     alert(' you picked a positive odd number ');
//     alert(' your number ' + userInput + ' plus 100 is ' + (Number(userInput) + 100))
//   }
//   else if  (userInput % 2 === 0 && userInput < 0) {
//     alert(' you picked a negative even number ');
//     alert(' your number ' + userInput + ' plus 100 is ' + (Number(userInput) + 100))
//   }
//   else if (userInput % 2 !== 0 && userInput < 0) {
//     alert(' you picked a negative odd number ');
//     alert(' your number ' + userInput + ' plus 100 is ' + (Number(userInput) + 100))
//
//   }
//
// }else
//   {alert(' maybe, some other time ')}


//    # 2
// Create a function named 'color' that accepts a string that is a color name as an input.
//  function color(String){
// This function should return a message that related to that color.
//   if(color === 'blue')
//  return "blue is the color of the sky"
//  if(color === 'red')
//  return "Roses are red"
// Only worry about the colors defined below, if the color passed is not one
// of the ones defined below, return a message that says so.
//  if(String !=== 'red' || 'blue')
//  return 'Sorry that is not a color')
//
//  if (String is === 'blue'){
//  return 'blue is the color of the sky'}
//  else if (String is === 'red'){ return 'Roses are red'}
// colors defined below, if the color passed is not one of the ones defined below, return
//    else {alert(' that is not a color ')}}
// a message that says so.

//   Ex
// color('blue') // returns "blue is the color of the sky"

// color('red') // returns "Roses are red"

// You should use an if-else if- else block to return different messages.

//   ***** SOLUTION PROBLEM  2

// function color(String){
//
//   if (String === 'blue'){
//     return ' blue is the color of the sky ';
//     }
//   else if (String === 'red'){
//     return 'Roses are red'
//     }
//   else {alert(' that is not a color ')
//   }
// }
// console.log(color('blue'))
//   ***



//   #3
// It's the year 2021, it's fall, and it's safe to shop again!
// Suppose there's a promotion at Target, each customer is given a randomly generated
// "lucky Target number" between 0 and 5.
// If your lucky number is 0, you have no discount,
// if your lucky number is 1 you'll get a 10% discount,
// if 2 is a 25% discount,
// if 3 is a 35% discount,
// if 4 is a 50% discount, and
// if 5 you'll get a 75% discount.


//  *** SOLUTION # 3

// function getRandomInt(max) {
//
//   const luckyRandom = Math.floor(Math.random() * Math.floor(max));
//   if(luckyRandom === 5){
//     alert('Your lucky number is ' + luckyRandom + ' the discount applied to your total will be 75')
//
//   } else if (luckyRandom === 4){
//     alert('Your lucky number is ' + luckyRandom + ' the discount applied to your total will be 50')
//
//   } else if(luckyRandom === 3){
//     alert('Your lucky number is ' + luckyRandom + ' the discount applied to your total will be 35')
//
//   } else if(luckyRandom === 2){
//     alert('Your lucky number is ' + luckyRandom + ' the discount applied to your total will be 25')
//
//   } else if(luckyRandom === 1){
//     alert('Your lucky number is ' + luckyRandom + ' the discount applied to your total will be 10')
//
//   } else {
//     alert('No discount applicable at this time')
//   }
//
// }
// console.log(getRandomInt(6))
//  ***

// Write a function named 'calculateTotal' that accepts a lucky number and total
// amount, and returns the discounted price.
//   function calculateTotal(max)
//   Ex
// calculateTotal(0, 100) // returns 100
// calculateTotal(4, 100) // return 50
// Test your function by passing it various values and checking for the expected return value.

//  ***** SOLUTION *******
//
// function calculateTotal(max) {
//
//   const luckyRandom = Math.floor(Math.random() * Math.floor(max));
//
//   let userInput = prompt(' Please enter your total amount, in order to apply your discount')
//
//   if (luckyRandom === 5 && userInput >= 75) {
//     alert('Your lucky number is ' + luckyRandom + ' the discount applied to your total will be 75')
//
//     alert('Hooray! your total amount is ' + (number(userInput) - 75));
//
//   } else if (luckyRandom === 5 && userInput < 75){
//     alert('Your lucky number is ' + luckyRandom + ' Sorry you need to go spend dollars ')
//
//   } else if (luckyRandom === 4 && userInput >= 50){
//     alert('Your lucky number is ' + luckyRandom + ' the discount applied to your total will be 50')
//
//     alert('Hooray! your total amount is ' + (number(userInput) - 50));
//   } else if (luckyRandom === 4 && userInput < 50){
//     alert('Your lucky number is ' + luckyRandom + ' Sorry you need to go spend dollars ')
//   }
// }
//
// console.log(calculateTotal())

//   ***





//   #4
// Create a grading system using if/else if/ else statements
// BONUS Use the switch statements
// 100 - 90 A
// 100 - 98 A+
// 97 - 94 A
// 93 - 90 A -
// 89 - 86 B+
//
// 79 - 70 C
// 69 - 60 D
// < 60 = F

//  **** SOLUTION PROBLEM  4 ****

// function gradingSystem() {
//     const grade = number;
//
//     let userInput = prompt('please enter in the grad to add the letter alue to grade report')
//
//   if (userInput >= 90){
//     alert(' A+ is a good letter to have an your grading report')
//   } else if (userInput >= 97){
//     alert(' A is okay')
//   } else if (userInput >= 94){
//     alert(' This is just a simple A')
//   }else if (userInput >= 93){
//     alert('This your 93 on down A')
//   }else if (userInput >= 89){
//     alert('B plus is the eway to go if you can\'t\n make an A' )
//   }else if (userInput >= 80){
//     alert('this is a simple B')
//   }else if (userInput >= 79){
//     alert('be careful you are now in the C\'s')
//   }else if (userInput >= 60){
//     alert('your letter grade is a D')
//   }else if (userInput < 60){
//     alert('your letter grade is an F')
//   }else {
//     alert('you need to go back and rethink your strategy')
//   }
// }
// console.log(gradingSystem(90))

//******

// SET THE Students grade / Switch

// // const grade = 90;
//
//   switch (true) {
//
//     case grade >= 90:
//       console.log('A');
//       break;
//
//     case grade >= 80:
//       console.log('B');
//       break;
//
//     case grade >= 70:
//       console.log('C');
//       break;
//
//     case grade >= 60:
//       console.log('D');
//       break;
//     default:
//       console.log('wrong grade');
//   }
// }


// # 5
// Create a time system using if/else if/else statement
// MILITARY TIME
// OR switch case
// < 12 = 'Good morning'
// < 18 = 'Good afternoon'
// *** something for 'Good evening'


//  ****** SOLUTION 5
// var militaryTime = 'Good'
//
// switch (militaryTime) {
//   case militaryTime <12 'Good':
//     console.log('Good Morning')
//       break;
//   case 'Good':
//     console.log('Good Afternoon')
// }

//    ****** SOLUTION #5

// function militaryTime(hour) {
//     if (hour <12){
//       alert('good morning');
//
//     }else if (hour < 18){
//
//       alert('good afternoon');
//     }else if (hour >18 && hour < 21){
//
//       alert('good evening');
//
//     } else {
//        alert('you dont know military time, do you?');
//}
// }
// militaryTime(11);
//   *****


//  #6

// Create a season system using if/else if/else statement
// EX. 'December - February = Winter', etc.

//  **** SOLUTION 6 ******

// function season(month) {
//
//     switch (month) {
//       case 'december':
//       case 'january':
//       case 'february':
//         alert((it\'s winter');
//         break;
//     }
//
// }
// season('january');

