"use strict"



const developers = [
  {
    name: 'stephen',
    email: 'stephen@appddictionstudio.com',
    languages: ['html', 'css', 'javascript', 'php', 'java']
  },
  {
    name: 'karen',
    email: 'karen@appddictionstudio.com',
    languages: ['java', 'javascript', 'angular', 'spring boot']
  },
  {
    name: 'juan',
    email: 'juan@appddictionstudio.com',
    languages: ['java', 'aws', 'php']
  },
  {
    name: 'faith',
    email: 'faith@codebound.com',
    languages: ['javascript', 'java', 'sql']
  },
  {
    name: 'dwight',
    email: 'dwight@codebound.com',
    languages: ['html', 'angular', 'javascript', 'sql']
  }

  {
    name: 'sandra',
    email: 'lnrs.sndr@gmail.com',
    languages: ['html', 'css', 'javascript']
  }
];


// // TODO: fill in your name and email and add some programming languages you know
// // to the languages array
// // TODO: replace the `var` keyword with `const`, then try to reassign a variable
// // declared as `const`
// const name = 'sandra';
// const email =
// const languages =
//
//   // function personInfo({name});
//   //   console.log(name);
//
//   // function developers({name, email, languages}){
//   // console.log(name);
//   // }
//
// // TODO: rewrite the object literal using object property shorthand
// developers.push({
//   name: name,
//   email: email,
//   languages: languages
// });
// console.log(developers);
//
// const
//
//
// // TODO: replace `var` with `let` in the following variable declarations
// let emails = [];
// let names = [];
//
//
// // TODO: rewrite the following using arrow functions
// developers.forEach(function(developer) {
//   return emails.push(developer.email);
// });
//
// developers.forEach(developer => eamils.push(developer.email));
//
// users.forEach(user => emails.push(user.email));
// console.log(emails);
// developers.forEach(function(developer) {
//   return names.push(developer.name);
// });
//
// developers.forEach(developer => names.push(developer.name));
//
//
// users.forEach(user => names.push(user.name));
// console.log(names);
//
// developers.forEach(function (developer) {
//   return languages.push(developer.languages);
// });
//
//
//
//
// // TODO: replace `var` with `const` in the following declaration
// var developers = [];
// developers.forEach(function(developer) {
//
//
//   // TODO: rewrite the code below to use object destructuring assignment
//   //       note that you can also use destructuring assignment in the function
//   //       parameter definition
//   const name = developer.name;
//   const email = developer.email;
//   const languages = developer.languages;
//
// const {name, email, languages} = developer;
//
//
//   // TODO: rewrite the assignment below to use template strings
//   developerTeam.push(name + '\'s email is ' + email + name + ' knows ' + languages.join(', '));
// });
// console.log(developers);
// developerTeam.push(name + '\'s email is ' + email + name + ' knows ' + languages.join(', '));
//
// // TODO: Use `const` for the following variable
// const list = '<ul>';
//
//
// // TODO: rewrite the following loop to use a for..of loop
// developers.forEach(function (developer) {
//
//
//
//   // TODO: rewrite the assignment below to use template strings
//   list += '<li>' + developer + '</li>';
// });
// list += '</ul>';
// document.write(list);
//
// for (const developer of developers){
//   // list +=  '<li> + developer '/li>';
//     list += ',li>${developer}</li'
//
//     list += '</ul';
//
// }
