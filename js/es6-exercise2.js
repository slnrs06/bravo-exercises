"use strict"
const petName = 'Chula';
const age = 14;



// REWRITE THE FOLLOWING USING TEMPLATE STRINGS
console.log("My dog is named " + name + ' and she is ' + age + ' years old.');

console.log('My dog is name ${petName} and she is ${age}years old.');
// const petName = Chula;
// const age = 14
//
// function petName(name, age) {
//
// }
// petName.push(`${name}`${age})


// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
function addTen(num1) {
  return num1 + 10;
}

const addTen = (num1) + 10;

// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
function minusFive(num1) {
  return num1 - 5;
}
const minusFive = (num1) => -5;



// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
function multiplyByTwo(num1) {
  return num1 * 2;
}

const multiplyByTwo = (num1) => *2;


// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
const greetings = function (name) {
  return "Hello, " + name + ' how are you?';
};

// const greetings = name =>
//     console.log('Hello ${name} how are you?');
// greetings('bob)



// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
function haveWeMet(name) {
  if (name === 'Bob') {
    return name + "Nice to see you again";
  } else {
    return "Nice to meet you"
  }
}
const haveWeMet = name => (name === Bob) ? 'Nice to see you again':
  console.log(haveWeMet('alex'));

//condition ? message a : message b

