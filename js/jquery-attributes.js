"use strict";

$(document).ready(function () {

  //   ATTRIBUTE METHODS

/*
    .html
    .css()
    .addClass()  - adds the specified class(es)
    .removeClass() - removes a single class, multiple classes or all classes
    .toggleClass() - add or remove one or more classes

  GETTERS/SETTERS


  METHOD CHAINING


 */
  // $('#codebound').click(function () {     // getter
  //   $(this).html('Codebound Rocks!');       //setter
  // });
  //
  // $('#name').css('color', 'firebrick').css('background-color', 'yellow');

//   var highlightSyling = {
//     'color': 'red',
//     'background-color': 'blue',
//     'font-size': '30px',
//
//   };
//
// $('#name').css(highlightSyling);

 //  .addClass()      //style from html  //// <div>
    //   SYNTAX:   .addClass( className);

  // $('.important').click(function () {
  //     $(this).addClass('highlighted');
  // });


 //   .removeClass()        /style from html  //// <div>

  // $('.important').dblclick(function () {
  //     $(this).removeClass('highlighted');
  // });


  ///  .toggleClass()           // do both add remove class

  // $('.important').click(function () {
  //     $(this).toggleClass('highlighted');
  // });







})

