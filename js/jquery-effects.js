"use strict";

$(document).ready(function () {
//  jQuery Effects
  /*
  .hide() - hides the elements(s)
  .show() - display the elements(s)
  .toggle() - displays/hides the elements(s)
   */

// When you click on 'Individual Artists', hide the listing of
// individual artists

  //  $(selector).click(handler)  //-handler is the annoymous function
  $('#music-artist-toggle').click(function () {
      $('music-artist').hide();
  });

$('#music-artist-toggle').hover(function () {
    $('music-artist').toggle();
});

//   ANIMATED EFFECTS  *******
  //Fading Effects
  //.fadeIn(). fadedOut() .fadeToggle()
  $('#music-artist-toggle').click(function () {
    $('#music-artist').fadeToggle(5000);

  });

  //  SLIDING EFFECTS
  //  .slideUp(). slideDown() .slideToggle()
  $('#music-band-toggle').click(function () {
    $('#music-bands').sliedDown(7500);
  });





});
