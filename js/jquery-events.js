"use strict";

// How to use jquery to attach event listeners to html elements
// -----Add jQuery code that will change the background color of a ‘h1’
// element when clicked.

// $('#name').click(function () {
//   $('#name').css('background-color', 'blue');
// })

//  ---- Make all paragraphs have a font size of 18px when they are double clicked.

// $('p').dblclick(function () {
//   $('p').css('font-size', '18px');
// })

//   ----Set all ‘li’ text color to green when the mouse is hovering,
//   reset to black when it is not.

// $('li').hover(
//
//    function () {
//         $(this).css('color', 'green');
//     },
//
//     function () {
//         $(this).css('color', 'black');
//     }
// );

//
// Use your jquery-exercises.html for this exercise.

//   Remove your custom jQuery code from previous exercises.

//  ---- Add an alert that reads "Keydown event triggered!" everytime
//   a user pushes a key down  // stephen answer

// $('*').keydown(function () {
//     alert('Keydown even triggered!');
// });



// ----Console log "Keyup event triggered!" whenever a user releases a key.
//stephen answer:

// $('*').keyup(function () {
// console.log('Keyup event triggered!')
//
// })

//   BONUS:
// ----Create a form with one input with a type set to text.

//   Using jQuery, create a counter for everytime a key is pressed.
// $('#entry').keydown(function () {
//     console.log(this.value.length);
// });
