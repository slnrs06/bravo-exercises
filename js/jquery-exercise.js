"use strict";

// $(document).ready(function () {
//   alert('The page has finished loading!');
// });


//jQuery Selectors  *********
// ID selector #id
// CLASS selector    .class  selelcts all elelments with given class name
// ELEMENT selector    element ex h1
// MULTIPLE selector
// ALL selector     *

//SYNTAX FOR jQuery Selectors   ************

//    $(selector)  /// selector is a string

// .html - returns the html contents of selected element
//  Similar to 'innerHTML' property

//  .css - allows us to change css properties for a element(s)
//  similar to the 'style' property

//  ID SELECTOR   ------

//  $('#id-name');

// var content = $('#codebound').html();
// alert(content);
// alert($('#codebound').html());

// CLASS SELECTOR   -------
//  SYNTAX for selecting  element with a class name:
//    $('.class-name');

// $('.urgent').css('background-color', 'red');  //single property

// ELEMENT SELECTOR  ------
// selects all elements if that same type
//  SYNTAX:   $('element_name')

// $('p').css('font-size', '50px');   // element selector

//  MULTIPLE SELECTOR
//   SYNTAX $('selector1, selector 2,....')  // inside set of single quotes

// $('.urgent,p').css('background-color', 'orange');  /multi selectors

//  ALL SELECTOR ---
//  SYNTAX:  $('*')

// $('*').css('border', '4px dotted red');     /// all selector









