"use strict";

// Create a new file named faq.html. Commit all changes to GitLab.
//   In a HTML structure, create a DEFINITION LIST containing 10 FAQs about a topic/subject/person/etc.
//   Add a class to all dd elements named invisible.
//


// $('dd').addClass('invisible');

//   Create CSS that sets elements with the invisible class to visibility: hidden;.


// $('.invisible').css('visibility', 'hidden');



// Update the page with jQuery to add a link that toggles the class invisible on and off for all dd elements.



// Include a navbar in your webpage with a logo that goes
// with the topic of your webpage.

//  Include a header with the name of the your website

// Include a photo that's related to your website.

// Include a footer that contains a copyright


// 1. Create a button that, when clicked, will change the 'answers'
// background-color to a color of your choice.



$('dd').click(function () {
     $(this).css('background-color', 'pink');
})




// 2. At the top of the page, add a div with two elements, an h1 that reads
// 'Would you like to sign up for more updates?' and a span that has an 'x' in
// it. When the 'x' is clicked, hide the entire div element.
$('.x').click(function () {
  $('#updt').hide();
});
// 3. After the user has been on the page for 10 seconds, an h3 element should fade
// in that asks 'Register to our site?'

$(document).ready(function () {

  $('#register').fadeIn(5000);

});

// $('#register').fadeIn();
// $('#register').fadeIn('slow');
// $('#register').fadeIn(5000);
// });

//BONUS
// Use a bootstrap alert for the dismissable message that shows on page load.
//
//   Use a bootstrap modal for the message that fades in after 8 seconds.
//   You may find that you can use bootstrap's modal methods to achieve the fading effect.
