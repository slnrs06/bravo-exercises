"use strict" //helps to create/write cleaner code
console.log("Hello from console"); //method used to test our work in cosole
console.log("hi")

//THIS IS A COMMENT
/*
A COMMENT
FOR MULTI LINES
 */



// DATA TYPES
//PRIMITIVE TYPES
//numbers, strings, boolean, undefined, null.....objects

//VARIABLES
//var, const

//NUMBERS - 100, -10,  0, 0.123
//STRINGS - "stephen", "email@example. com", "qweruio"
//BOOLEAN - true, false
//if a user is twentyOne or Older?  true or false
//undefined - unassigned variables have this value and type
//var person = undefined;
//NULL - a special keyword for nothing


//         ****OPERATORS******
//built-in operators
//Math.cos(9);
//Math.pow(4,2);
//Basic arithmetic
/*
  + (add)
  - (subtract)
  * (multiply)
  / (divide)
  % Modulus aka remainder
*/
//(1 + 2) * 4 / 5;
//3 * $ /5

//      *** LOGICAL OPERATORS ***
// returns a boolean value when used with boolean values
//also used with non-boolean values

/*
&& (AND)

|| (OR)
!  (NOT)
 */

/*
&& (AND)
- A user can edit a message if they are the admin && the author;
INPUT      OUTPUT
T && T      TRUE
T && F      FALSE
F && F      TRUE
F && T      FALSE


|| (OR)
- A user can edit a message if they are a registered user || the admin;
INPUT       OUTPUT
T || T       TRUE
T || F       TRUE
F || F       FALSE
F || T       TRUE

!    (NOT)(the opposite)
Redrirect users if they are ! logged in;

INPUT       OUTPUT
!T           FALSE
!F           TRUE

EXAMPLE

false && false || true :  true
true || false || true:  true
true && false && false || true :  true

// ****** COMPARISON OPERATORS*****
// WILL RETURN A LOGICAL VALUE BASED ON WHETHER THE COMPARISON IS TRUE
// can be any data type
// 4 Comparison Operators

==   :check if the value is the same
===  : check if the value and the data type are the same
!=   : check if the value is NOT the same
!==  : check if the value and data type are NOT the same

EXAMPLES

4 == 4   :TRUE
4 === '4' :FALSE
4 != 5    :  TRUE
4 == '4'  :  TRUE
4 == 5    : FALSE
4 !== 4   : FALSE
4 !== '4'  :TRUE

7 !== 5   :TRUE
7 != '5'  :TRUE
4 != '2'  :TRUE
4 !== '2' :TRUE
4 !== FALSE : TRUE
4 !== 4  : FALSE

<, >, <=, >=
CONST AGE = 21;

age < 18 :  false
(21) < 18 : false

age > 18 : true
age > 21  : false
age >= 21 : true

age >= 21 || age < 18 : True
     T     ||   F
age <= 21 || age > 18: T
     T         T
age  <21 && age > 18 :  F
     F         T




*/
// CONCATENATION

/*const name = 'stephen';
const age = 13;

console.log(name);
console.log(age);

console.log('Hello, my name is' + name + ' and I am' + age);


//TEMPLATE STRING
/*
` key next to 1 */

/*console.log(`Hello, my name is ${name} and I am ${age}`);

//GET THE LENGTH OF A STRING
consule.log(name.length);

const s = 'Hello World'
console.log(s.length);

//GET THE STRING IN UPPERCASE
console.log(s.toUpperCase());

// GET THE STRING LOWERCASE
console.log(s.toLowerCase());

// INDEX
console.log(s.substring(0, 5)); // Hello
console.log(s.substring(6, 11)); //World

// USER INTERACTION
// ALERT ()
//console.log('message')
alert('Hello there!');
alert('Welcome to my website');

//confirm
var confirmed = confirm('Are you sure you want to continue');
console.log(confirmed);

// prompt
var userInput = prompt('Enter your name: ');
console.log('The user enter:'+ userInput);
//

//   ***FUNCTIONS***
// FUNCTION  - is a resuable block of code that performs
// a specified task.

//  SYNTAX FOR A FUNCTION
/*
function name of the Function(parameters1, and/or arguments1) {
    code you want the function to do
}

 //  CALL A FUNCTION
 //  call it by its name with ()
  nameOfTheFunction();   call the function

var result = nameOfTheFunctio();
console.log(result);

nameOfTheFunction;  this will NOT run the function

 */

//function increment(num1) {
 //   return num1 + 1;
//}

//  call the function  just enter number within parenthesis
//console.log(increment(2));
//  NaN - Not a Number
// function increment (4) {
// return 4+1; // 5
//}

//var five = increment(4);
//console.log(five); // 5

//   ANONYMOUS FUNCTIONS
//  Including a name for our function is acutally optitonal
// To define a function we can also create an anonymous function.
// Function without a name, and store it in a variable

//  SYNTAX FOR ANONYMOUS FUNCTION
//var increment = fuction (num1){
//  return num1 + 1;
//};
//
//  CALLING AN ANONUYMOUS FUNCTION
//var two = increment(num1+1);
//console.log(two); //2

// ARGUMENTS / PARAMETERS
// argument = the value that a function is called with
// parameter= part of the functions definition

//var two=increment(1)
//
//function sum(a,b,);
 //  return a + b
//console.log(sum(a:10, b: 51));

//function sumPart2(a,b);
//  var result = a+b;
//     return result;
//}
// console.log(sumPart2(a: 10, b:51));

//  A FUNCTION WITH A RETURN VALUE

//function shout(message) {
//    alert(message.toUpperCase() + '!!!');
//}
//var returnValue = shout('hey yall');
//console.log(returnValue);
// HEY YALL!!!
//  function with no paremeters and no return value
//function greeting() {
 //   alert('hey there!');
//}
//console.log(greeting());

//function greeting(message){
//  return message;
//}
//console.log(greeting('Hey Codebound!'));

//  SHORTHAND ASSIGNMNT OPERATORS

//y * x = x;

//Operator     Original          Shorthand
// +=           x + 1 = x;           x += 1;
// -=           x - 1 = x;           x -= 1;
// *=           x * 1 = x;          x *= 1;
// /=           x / 1 = x;          x /= 1;
// %=           x 5 1 = x;          x 5= 1;

// Unary Operators
//  +    plus
//  -    negative

//  ++   (increment)  pre, post
//  --    (decrement)  pre, post

// Increment
//   var x = 2;
// ++x; ||  output: 3 pre increment   ** increase by 1

// x++; ||  output:2  post increment   ** remain same 2

//  var = 4;
//  --x;;|| output  3   decrease 1
//  x--||  output   4   same  4


//   function f() {        ******

//}
//function f(input) {        ***
//    var output;
//    return output;

//}


//function do(input) {       ***
 //   var output = input
 //   return output
//}

// var let

//   FUNCTION SCOPE
// Block(local) vs Global variables        ****

//BLOCK(local) Variables
//function ex(){
//  var example = "Hello, My name is karen"  // Local
//}
// GLOBAL variable
//var example = "Hello, My name is karen"  // Global
//function ex(){
  // do something here
//}

//
//* ******TODO:    ******
//* Write a function named isOdd(x).
//* This function should return a boolean value.
//**/
//function isOdd(x) {
//    return x % 2 !== 0;
//5% 2 !== 0;
//}
//console.log(isOdd(1));

//  ***** CONDITIONAL STATMENTS ******

// IF STATEMENTS
/**
var color = 'pink';
 switch (color) {
    case 'blue':
    console.log('You chose blue')
      break;
    case 'red':
    console.log('You chose red')
      break:
    case 'pink'
      console.log('Pink is the best color in the World');
     default:
      console.log('You chose the wrong color');
 }

switch

 ****   Ternary Operators
 shorthand way of creating if/else statments
 //only used when there are only two choices
 BASIC SYNTAX
  (if this condition is true) ? run this code : otherwise run this
    code instead
 **   Example: Ternary Operators
 var numberofLives = 0;
 (numberofLives ==== 0) ? console.log("Game Ove") : console.log('I\'m still alive');

**/
/**



            *** LOOPS NOTES *****
While Loop, Do-While, For Loop, Break and Continue


 While loop  -
 is a basic looping construct that will execute a
 block of code as long a certain condition is true.
 SYNTAX:
 while ('condition'){
  //  run the code
 }

 var n = 1;
 while (n<10) {                   /or while (n<=10)
 console.log('#' + n);
   n++:
 }

DO WHILE
the only difference from a while loop, is that the
condition is evaluated at the end of the loop.
Instead of the beginning.

 SYNTAX:

 do {
   run the code

 } while ('condition')

 var i = 10;
 do {
    console.log('Do-While #' + i);
    i++;
 }while (i <= 20);

 ***  FOR LOOPS ****
 A For Loops is a robust looping mechanism available in
 many programming languages.

 ** SYNTAX **
 for ('initalization'; 'condition'; 'increment(++)/decremnt(--)') {
      run the code
 }
      (var x = 10; x <= 100; x ++){
 for (var x = 10; x <= 100; x --){            /infinite loop
      console.log('For Loop#' + x);
 }
 ***** BREAK AND CONTINUE ******
  - breaking out of a loop
  using the 'break' keyword allows us to exit the loop

  var endAtNumber = 5;
  for (var i = 1; i < 100; i++){
       console.log('Loop count #' + i);

   if (i === endAtNumber){
   alert('we have reached the stopping point!');
   break;
   console.log('do you see this message?');
   }
  }
  Continue keyboard
  using CONTINUE keyboard


for (var n = 1; n <= 100; n++){
if (n % 2!== 0) {
continue;
}
console.log('Even number : ' + n);
}

**/

//                    ***** ARRAYS ****

//     IIFE  - Immediatetly Invoked Function Expression
//  a way to execute code immediatetly
//  BASIC SYNTAX
//  (function(){
// content/code goes here
//}())

// ON HTML
// ARRAYS

//  --     they are used to store value in a variable
//  any type of the data
//  they can hold









//   JS PAGE
//  (function (){     IIFE GOES FIRST
//"use strict"        STRICT GOES NEXT
// Basic Syntax

// []  // an empty array
// ['pink', 'purple', 'blue', 'black']  // each individual value is considered an elements
//
//   ['pink', 'purple', 'blue', 'black'] // array with 4 elements

// [4,6,8,10,12,]    array of numbers
// [2,3,4,5,[2,4,,6,8]]  store arrays inside arrays / nested arrays
// var colors = ['black', 'green', 'blue', 'purple', 'yellow']
//     console.log(colors);
//         console.log(colors.length);    // length of array

// -      Accessing arrays  --------------------
//   zero-indexed
//     var codebound = ['stephen','faith','karen','leslie', 'twyla']
//
//     console.log(codebound[4]); twyla
//     console.log(codebound[3]);  leslie
//
//     console.log(codebound[5]);    /// undefined
//
 //       console.log(codebound)

// console.log('there are ' + codebound.length + ' people on the codebound team');

//      ****ITERATING ARRAY **** -------------------
// --- travese through elements // traveling
// var codebound = ['stephen','faith','karen','leslie', 'twyla']
//
// //  loop through an array and console.log values  ----
//
// for (var i = 0; i < codebound.length; i += 1);{
//   console.log('here is everyone on codebound team: ' + codebound[i]);
//   console.log( codebound[i]);
// }



//  //          forEach Loop  ----
//    BASIC SYNTAX
//       **      array.forEach(function (element, index, array) { //    function is the call back function
//
// })

//  ---  keep naming conventions to plural nouns -----
// var numbers = [2,3.5,6,7]   // each number element is a number

// var colors = ['blue', 'black', 'purple', 'green']
//
// colors.forEach(function (color) {          // console log the color of var string
// // ----- do something
//   console.log(color)


//})

//   -----  CHANGING / MANIPULATING ARRAYS -------

var fruits = ['banana', 'apple', 'grape', 'strawberry']

// adding to ARRAY
console.log(fruits)

// .push      ///allows to push into array// adds to the end of an ARRAY

fruits.push('watermelon');
console.log(fruits)            ////adding to an ARRAY...at the end

//  .unshift adds to the beginning of an ARRAY
fruits.unshift('cherry')
console.log(fruits);

fruits.push('mandarin', 'dragonfruit', 'starfruit')
console.log(fruits);


//  REMOVE / DELETE FROM AN ARRAY -------
// .pop // will remove an ARRAY
fruits.pop();  /// remove the last element in the array  --
console.log(fruits);

// .shift  // remove first element in the array  --
fruits.shift();
console.log(fruits);

var removedFruit = fruits.shift();  /// storing in a variable
console.log('here is the fruit i removed: ' + removedFruit)

//  -----------LOCATE ARRAY ELEMENTS
var fruits = ['banana', 'grape', 'apple', 'grape', 'strawberry']

//     .indexOf       // returns first occurance of what you are lookin for in index
var index = fruits.indexOf('grape')     /// returns first occurance looking for
console.log(index);


//     .lastIndexof  starts at the end of an ARRAY and returns in  ------
index = fruits.lastIndexOf('grape')
console.log(index)

//          SLICING ------   copys a portion of an ARRAY
//   .slice  // copies a portion of the array
// return a new array - does not modify original array

var fruits = ['banana', 'grape', 'apple', 'grape', 'strawberry', 'mandarin', 'dragonfruit']

var slice = fruits.slice(2,5);
console.log(slice);

slice = fruits.slice(4);
console.log(slice);


/// ----  REVERSE ----  reverses order of ARRAY
//  .reverse     will reverse the original ARRAY AND RETURN THE REVERSES ARRAY.

var fruits = ['banana', 'grape', 'apple', 'grape', 'strawberry', 'mandarin', 'dragonfruit']

fruits.reverse();
console.log(fruits);


/// - SORTING --------
//   .sort       will sort the original array AND return the sorted array  //// returns alphbtclly

// var fruits = ['banana', 'grape', 'apple', 'grape', 'strawberry', 'mandarin', 'dragonfruit']
//
//
// fruits.sort();
// console.log(fruits);
//
//
// //   SPLITTING -------
//
// //  .split
// //  takes a string and turns into an ARRAY
//
// var codebound = 'karen, faith, stephen';       /// STRING
// console.log(codebound);
//
// var codeBoundArray = codebound.split(',')   /// IN AN ARRAY
// console.log(codeBoundArray);
//
// //  JOINING  --------
// // .join
// //  takes an array and converts it into a String with delimiter of your choice
//
// var codeBoundArray = ['karen', 'faith', 'stephen'];
// console.log(codeBoundArray);
// var newCodeBound = CodeBoundArray.join(',')
// console.log(newCodeBound)


///










// })()










