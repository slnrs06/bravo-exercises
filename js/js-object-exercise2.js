// "use strict"
//
//
//
// const albums = [
//    {artist: 'Michael Jackson', title: 'Thrillers', released: 1982, genre: 'Pop'},
//   {artist: 'AC/DC', title: 'Back in Black', released: 1980, genre: 'Rock'},
//    {artist: 'Pink Floyd', title: 'The Dark Side of the Moon', released: 1973, genre: 'Rock'},
//    {artist: 'Bee Gees', title: 'Saturday Night Fever', released: 1977, genre: 'Disco'},
//    {artist: 'Fleetwood Mac', title: 'Rumours', released: 1977, genre: 'Rock'},
//    {artist: 'Shania Twain', title: 'Come On Over', released: 1997, genre: 'Country'},
//    {artist: 'Michael Jackson', title: 'Bad', released: 1987, genre: 'Pop'},
//    {artist: 'Led Zeppelin', title: 'Led Zeppelin IV', released: 1971, genre: 'Rock'},
//    {artist: 'The Beatles', title: '1', released: 2000, genre: 'Rock'},
//    {artist: 'Whitney Houston', title: 'Whitney', released: 1987, genre: 'Pop'},
//   {artist: 'Def Leppard', title: 'Hysteria', released: 1987, genre: 'Rock'},
//    {artist: 'Tupac', title: 'All Eyez on Me', released: 1996, genre: 'Rap'},
//    {artist: 'Eminem', title: 'The Marshall Mathers LP', released: 2000, genre: 'Rap'},
//    {artist: 'Green Day', title: 'Dookie', released: 1994, genre: 'Rock'},
//    {artist: 'Michael Jackson', title: 'Dangerous', released: 1991, genre: 'Pop'},
//  {artist: 'The Notorious B.I.G', title: 'Ready to Die', released: 1994, genre: 'Rap'},
//  {artist: 'Adele', title: '21', released: 2011, genre: 'Pop'},
//   {artist: 'Metallica', title: 'Load', released: 1996, genre: 'Rock'},
//    {artist: 'Prince', title: '1999', released: 1982, genre: 'Pop'},
//   {artist: 'Lady Gaga', title: 'Born This Way', released: 2011, genre: 'Pop'}]
//
// // 1. create a for loop function that logs every 'Pop' album
//
// for (var i = 0; i < albums.length; i++){
//   if(albums[i].genre === 'Pop'){
//     console.log(albums[i].title)
//   }
// }
//
// // 2. create a for each function that logs every 'Rock' album
//
// albums.forEach(function (albums, x) {
// if (albums[x.genre === 'Rock']){
//   console.log(albums[x].title)
// }
//
// })
//
//
// // 3. create a for each function that logs every album released before 2000
//
// // albums.forEach(function (albums, index) {
// //  if (albums[i].released < 2000){
// //    console.log(albums[i].released)
// //  }
// // })
//
// albums.forEach(function (album) {
// if (album.release < 2000){
//   console.log(album.release + ' '+ album.title)
// }
//
// })
//
//
// // 4. create a for loop function that logs every album between 1990 - 2020
//
// for (var i = 0; i < albums.length; i++){
//   if (albums[i].released >= 1990 && albums[i].released < 2020){
//     console.log(albums.released[i].artist + ' ' + albums[i].title);
//   }
// }
//
//
// // 4. for loop function that logs every Michael Jackson album
//
// for (var i = 0; i < albums.length; i++){
//   if(albums[i].artist === 'Michael Jackson'){
//     console.log(albums[i].title)
//   }
// }
//
//
// // 5. create a function name 'addAlbum' that accepts the same parameters from
// // 'albums' and add it to the array
//
// function addAlbum(artist, title, released, genre) {
// albums.push(
//   {
//     'artist': artist,
//     'title': title,
//     'released': released,
//     'genre': genre,
//   }
//
//
// )
// }
//
// addAlbum('Judas Priest', 'Hell Bent', '1982', 'Rock');
// console.log(albums);
//


// ** BONUS:
//   * Loop through the movies array and output the following information about
// * each book:
//   * - the movie number (use the index of the movie in the array)
// *
// *
// * Example Console Output:
//   Movie # 1
// Title: Batman
// another property
// another property
// *      ---
//   Movie # 2
// Title: 1917
// another property
// another property
// *      ---
//   Movie # 3
// Title: Galaxy Quest
// another property
// another property
// *      ---
