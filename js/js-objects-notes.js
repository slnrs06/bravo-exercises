"use strict"

  // ----  OBJECTS ----
//Object is a grouping of data and functionality.
// --- single var line
// var firstName = 'Stephen';
// var lastName = 'Guedea';
// var age = '30';
// var address = '122';

///  Data items inside of an object  = PROPERTIES
//  Functions inside of an object = METHODS

//    CUSTOM OBJECTS
// Prototypes allows exisiting objects to be used as templates to
//  create new objects.
//  Object keyword - the starting point to make custome objects.
//  EX:
//  NEW OBJECT INSTANCE ----
// var car = new Object();
// console.log(typeof car);
// object     ----chrome
// The use of a 'new Object()' calls the Object CONSTRUCTOR to build
// a new INSTANCE  of a new object.

// OBJECT LITERAL NOTATION
//  - CURLY BRACES {}

// var car = {};
// alert(typeof car);
// object  --chrome

//  SETTING UP PROPERTIES ON A CUSTOM OBJECT  -----
// var car ={};
// // dot notation
// car.make = 'Toyota';          /// make is the Property
//
// //  array notoation
// car['model'] = '4Runner';
//
// console.log(car);       //{make: "Toyota", model: "4Runner"}

//  Another way/ most  common way to assign properties

// var car = {
//   make: 'Toyota',
//   model: '4Runner',
//   color: 'Black'
//   numberOfWheels: 4
// };
// console.log(car);
// //
// {make: "Toyota", model: "4Runner", color: "Black"}
// color: "Black"
// make: "Toyota"
// model: "4Runner"

// DONT DO THIS   ********
// car ['numberOfDoors'] = 5

// INSTEAD
// car.numberOfDoors = 5;
// console.log(car);
//console.log(car.model)


//   NESTED VALUES  ///arrays       // {} empty object
// var cars = [          //array
//   {
//     make: 'Toyota',
//     model: '4Runner',
//     color: 'Black',
//     numberOfWheels: 4,
//     features: ['automatic doors'.'bluetooth']
//   },                   // object    /// this object index is 0
//   {
//     make: 'honda',
//     model: 'civic',
//     color: 'green',
//     numberOfWheels: 4,
//     features: [],    /// array
//     owner: {       // object  /// this object is index 1
//       name: 'john doe',
//       age: 35
//     }
//   }
// ];        //  array
// console.log(cars[0].color);       /// color black of toyota
// console.log(cars[0].features[0]);
// console.log(cars[0].features);

// console.log(cars[1].owner.age);     /// age of owner

//DISPLAY THE FEATURES FOR BOTH CARS

//   ******
// cars.forEach(function (car:) {
//   car.features.forEach(function (feature:) {
//
//     console.log(feature);
//   })
//
// })
//   ******

// console.log(cars[1].alarm());

//*****   notes REMEMBER FUNCTIONS
// function sayHello() {
//   console.log('Hello')
// }
// // call the functiton's name
// sayHello();





















