(function () {
  "use strict"

  //  -----   BOM LECTURE NOTES  *****
  //    ----- Browser Object Model
  // hierarchy of objects in the browser.
  //  we can target and manipulate HTML elements with JavaScript

  //  ----- Makeup the BOM  ----
      // -------Location ---
          //   LOCATION OBJECT//
      //  manipulate the location of a object using the location object
        // get query stirng parameters

      //   ------Navigator  ---
       //  -- navigator object
        //  query info and capabilities of the browser

     //   ------ Screen-----
       //   screen object
        //  get info about the screen using screen object

     //   ------ History ------
        // history object - gets info/manage the web browsers screen

     //   *** Window ------

     //  *** Document --  DOM  ***

  //  ---- *****    -- Window Object ***  // core of BOM -- web browser

     // ---- Window object represents the JS global object
     // all variable and functions declared globally with the VAR keyword
     //  become the properties of the window object.

    // --- displayed in window object:
  // alert()
  // confirm()
  // prompt()

  // setTimeout()//  allows to set a timer and execute a callback function once
  // //the timer expires.
  //
  // setInterval() // execute a callback function REPEATLY with a fixed delay
  // //between each call.

  // // ----- *** setTimeout() *** -------

  // Basic Syntaxt:
  //  var timeoutId = setInterval(callback[DELAY], arg1, arg2,.....);
  //                           cb function, timer,
  // delay is in milliseconds
  //  delay will default to 0

  //EXAMPLE:

  // function timeoutEx() {
  //  setTimeout(function () {
  //   alert('Hello Bravo Cohort and welcome to Codebound')
  //  },7000);       /// set DELAY in milliseconds- automatically stop
  // }
  //
  // timeoutEx();           /// run timeout example

  //  ------ ****** setInterval() -----

  //  basic syntax

  // var intervalID = setInterval(CB,delay,arguments[arg1,arg2,...])
  // // CB is executed every delay millisecond -- is in time
  // // DELAY is the time that the timer should delay between executions of
  // // of the callback function
  //
  // // clearInterval()    -- in order to stop // ALLOWS TO STOP
  // clearInterval(intervalID);

  // EXAMPLE:

  // function intervalEX() {
  //    setInterval(function () {
  //     alert('HiFaith')
  //    },3000)
  // }
  //
  // intervalEx();          ///RUN it--  CALL IT
  // clearInterval();       //


  // var intervalID() {
  //    setInterval(function () {
  //     alert('HiFaith')
  //    },3000)
  // }
  //
  // intervalEx();          ///RUN it--  CALL IT
  // clearInterval(intervalID);       // stop running  ***


  // -----  Example :   INTERVAL ****

  // var count = 0;
  // var max = 10;    /// counted up to 10
  // var interval = 2000;       // specified a millisecond
  //
  // var intervalID = setInterval(function () {
  //     if (cont >= max){
  //       clearInterval(intervalID);
  //       console.log('finished')
  //     }else {
  //       count++;
  //       console.log(count);
  //     }
  //
  // },interval);
  //


  //  ---- DOCUMENT OBJECT MODEL *****  /child of the BOM
  // allows to manipulate HTML elements
  //  manipulate HTML using Javascript.    / .getElement

  //   ****** Locating the elements *****
          // target HTML by:
            // Element
              //  Class
             //  Id

    //access docuemnt object model

  // get ElementBy///
  //Basic Syntax
 // document.getElementsByName('Name of the Element/Class/Id');

  // in the HTML page   <button id='btn1'>Click Me</button>  ///HTML page

  //EXAMPLE --- with HTML PAGE  ***  by element
  // var btnClicked = document.getElementById('btn1');
  // console.log(btnClicked)


  // var btnClicked = document.getElementTagname('btn1');
  // console.log(btnClicked)

    //  ----- Accessing Form Inputs -----

  //  we access forms using the form collection ----
//   var usernameInput = document.forms.login.username;//name of the form on html//
// console.log(usernameInput);


  //  ---------  Accessing HTML Elements Using Class -------
  var cards = document.getElementsByClassName('card') // targeting group all the cards

  console.log(cards);

  // cards.pop();   // remove the last element of array * doesnt work cuz html colleciton
  //cards.shift();   // remove first element of array *//doesnt work cuz html colleciton
  // console.log(cards);
  console.log(cards[3]);  // access directly an array// above not working*

var cardsArr = Array.from(cards);  /// saved html collection into an array
  //convert html collecition to array

  cardsArr.shift();
  console.log(cards.Arr);

  cardsArr.pop();
  console.log(cardsArr)


  //  ---------  Accessing HTML Elements Using Tags ------
  //   section Tag

  var setions = document.getElementsByName('section') //accessing the DOM
console.log(sections);

  // ----         QuerySelector()   _______
  //returns the first element within the document DOM that mathes specified
  //selector or group

  var headerTitle  = document.querySelector('header h1');
  console.log(headerTitle);

  var headerTitle = document.querySelector('#main-title');
  console.log(headerTitle);

  // ***** querySelectorAll(); ***  // returns node list// collection or list

  var cards = document.querySelectorAll('card')

  // --------   Accessing FORM

  var feedbackForm = document.forms['login']  /// access direct login form []


  //  -----  ACCESSING AND MODIFYING ELEMENTS AND PROPERTIES  -----

  // GET VALUE OF INNER HTML

  var title = document.getElementById('main-title');

  console.log(title.innerHTML);
  // returns inner part of html
// inner part of main title on html

  // **  set the value of innerHTML  // target main title id and change it to:

  title.innerHTML = "Hi <em></em>Stephen</em>!"
// console.log()
 //      set the value of innerText ---

  var title = document.getElementById('main-title');
  console.log(title.innerText);    ///title

  title.innerText = "Hi <em></em>MaryAnn</em>!"  //reading innerText

// ---  append value to innerText (works the same with innerHTML)


    title.innerHTML +=  "Hi Stephen!"
//  +=   shorthand operators///  added stephen to current title

///  -----  ACCESSING AND MODIFY USING ATTRIBUTES ------

  // check if attribute exists

      var form = document.forms['login'];
      console.log(form);

//target specific attribute
    console.log(form.hasAttribute('action'))
// returns boolean value - true

  // get an attribute value
      console.log(form.getAttribute('method'));
  // returns value of Post
  // action attribue is nothing " "// returns nothing

  // create a new attribute or change a value of an existing attribute
      form.setAttribute('id', 'feedback-form')
// added an attribute of feedback-form

    // changes existing attribute//attribute in 'method'
      form.setAttribute('method', 'GET');

  //  ---  delete attributes
    form.removeAttribute('action');
    console.log(form);


  //  ---- Accesing and modifying

  // single style  *****

      var jumbotron = document.querySelector('jumbotron');
      jumbotron.style.display = 'none';  //// removes title

      jumbotron.style.fontFamily = 'Comic Sans Ms'  /// styling for font

   //  multiple styles ****** //  target the object not document

      Object.assign(jumbotron.style,{            // adding mutli styles
        border: "10px solid black",
        fontFamily: "Trajan",
        textDecoration: "underline"
      });

  //  Styling Node list   //// collection or list of resembles an array

    var tableRows = document.getElementsByTagName('tr');
    console.log(tableRows);
  //
  //   for (var i = 0, i< tableRows.length; i +=1){
  //     tableRows[i].style.background - 'hotpink';
  // }

    // tableRows.forEach(function () {   // will not work on HTML collecitons
    //       tableRows.style.background = 'lightblue'
    // });
    var tableRowArr = Array.from(tableRows);
     tableRows.forEach(function (tableRows) {
        tableRows.style.background = 'blue'
     })

  //  adding and removing elements

  //    createElement()

  //   removeChild()

  //  appendChild()

  // replaceChild()











})

