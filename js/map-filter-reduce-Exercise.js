"use strict"
console.log('hello');


const developers = [
  {
    name: 'stephen',
    email: 'stephen@appddictionstudio.com',
    languages: ['html', 'css', 'javascript', 'angular', 'php'],
    yearExperience: 4
  },
  {
    name: 'karen',
    email: 'karen@appddictionstudio.com',
    languages: ['java', 'javascript', 'spring boot'],
    yearExperience: 3
  },
  {
    name: 'juan',
    email: 'juan@appddictionstudio.com',
    languages: ['html', 'css', 'java', 'aws', 'php'],
    yearExperience: 2
  },
  {
    name: 'faith',
    email: 'faith@codebound.com',
    languages: ['node', 'npm', 'sql', 'javascript', 'java' ],
    yearExperience: 5
  },
  {
    name: 'dwight',
    email: 'dwight@codebound.com',
    languages: ['html', 'angular', 'javascript', 'sql'],
    yearExperience: 8
  }
];

// /**Use .filter to create an array of developer objects where they have
//  at least 5 languages in the languages array*/
// const languages = ['html', 'css', 'javascript', 'angular', 'php']



const developerswithMoreThanFiveLanguages = developers.filter(developer =>
developer.languages.length >= 5);

console.log(developerswithMoreThanFiveLanguages);




// /**Use .map to create an array of strings where each element is a developer's
//  email address*/

// var increment = numbers.map(function (num) {
//     return num + 1;
// });
// console.log(increment);
//
const developersEmail  = developers.map(developer => developer.email);

console.log(developersEmail)




// /**Use reduce to get the total years of experience from the list of developers.
//  - Once you get the total of years you can use the result to calculate the average.*/
//

const totalYearsExp = developers.reduce((totalExp, developer) =>
{
  return totalExp + developer.yearExperience;

}, 0);
console.log(totalYearsExp);

const average = totalYearsExp / developers.length;
console.log(average);


// /**Use reduce to get the longest email from the list.*/

const longestEmail =  developers.reduce((a, b) => {

  return a.email.length > email.length ? a : b;
}).eamil;
console.log(longestEmail);




// /**Use reduce to get the list of developer's names in a single string
//  - output:
//  CodeBound Staff: stephen, karen, juan, faith, dwight*/

const totalStaff = developers.reduce((names, staff) =>  {
  return names + staff.name;
  // return `${names}${staff.name},`;     //template string
}, 'Codebound Staff: ');
console.log(totalStaff)




// /** BONUS: use reduce to get the unique list of languages from the list
//  of developers
//  */









