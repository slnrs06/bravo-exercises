"use strict"

// CREATE A FILE NAMED 'promises-exercise.js' INSIDE OF YOUR JS DIRECTORY

// Write a function name 'wait' that accepts a number as a parameter, and returns

// a PROMISE that RESOLVES AFTER THE PASSED NUMBER OF MILLISECONDS

// EX. wait(4000).then(() => console.log('You will see this after 4 seconds.'));
// EX. wait(8000).then(() => console.log('You will see this after 8 seconds.'));


function wait() {
}
const wait = (delay) => {
  return new Promise ((resolve,reject) => {
    setTimeout(()=> {
      resolve(`you will see this after'
       ${delay / 1000 } seconds`);
    }, delay);
    });
  };
wait(4000).then((message) => console.log(message));



//Exercise :
//Write a function testNum that takes a number as an argument and returns
// a Promise that tests if the value is less than or greater than the value 10.
