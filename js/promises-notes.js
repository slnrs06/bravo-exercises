"use strict"

//*PROMISE * placeholder for something is going to happen later on
//  JS OBJECT
// CONTAINER FOR A FUTURE VALUE
// BE FULFILLED AND REJECTED
//  PENDING RESOLVED AND REJECTED
///   SYNTAX:    FETCH  .THEN  .CATCH
//  CHAIN TOGETHER     .THEN


//  We can create a PROMISE object like so:  resolve and reject example ------

// const myPromise = new Promise((resolve, reject) => {
//   if (Math.random() > 0.5){
//     resolve();
// }  else{
//   reject();
// }
// });
// myPromise.then(() => console.log('resolved'));
// myPromise.catch(() => console.log('rejected'));


//
// const myPromise = new Promise((resolve, reject) => {
//   if(Math.random()>0.5){
//     resolve();
//   } else{
//     reject();
//   }
// });
// myPromise.then(() => console.log('resolved'));
// myPromise.catch(() =>  console.log('rejected'));

//  --- Discovering pending
//
// const myPromise = new Promise((resolve,reject) => {
//   setTimeout(() => {
//     if(Math.random()>0.5) {
//       resolve();
//          }else{
//            reject();
//
//         }
//
//       }, timeout:1500);
//
//   });
// console.log(myPromise);    // a pending promise
// //
// // myPromise.then(() => console.log('resolved');
// myPromise.catch(() => console.log('rejected')):
//
// //  Write a promise that simulates and api request
//
// function makeRequest(){
//   return new Promise((resolve,reject) =>{
//     setTimeout(() => {
//       if (Math.random() > 0.1) {
//         resolve('Here is our data');
//
//       } else {
//         reject('Network connection error');
//
//       }
//     }, 1500);
//
//     });
//
// }
// const request = makeRequest();
// console.log(request);
// request.then(message => console.log('Promise resolved', message));
//
// request.catch(message => console.log('Promises rejected', message));



// const data ={username: 'example'};    /// create data of object example    value - data
//


fetch('data/starWars.json', {
  method: 'POST',           /// post data for you  key value pairs
  headers: {
    'Content-Type':'application/json',
  },
  body: JSON.stringify(data),   /// pulling value
})
  .then(response => response.json())   //then give me back my response
  .then(data => {
    console.log('success',data)
  })
  .catch((error))=> {
  console.log('Error fetch gone back', error);
})

// ***************
//
// const isMomHappy = true;
//
// //  create a Promise in js
//
// const willGetNewPhone = new Promise(((resolve, reject)=>
//    {if(isMomHappy){
//     const phone = {        // part of promise
//       brand: 'Samsung',
//       color: 'Black'
//     };
//     resolve(phone);
//
// }else {
//     const reason = new Error('mom is not happy')
//     reject(reason);
// }}))
//
// //  create 2nd PROMISE
//
// const showOff = function(phone){
//   const message ='hey friend i have a new' +     /// object variable
//     phone.brand + '' + phone.color + 'phone';
//       return Promise.resolve(message);
// }
// //  CALL our PROMISE
// const askMom = function(){
//   willGetNewPhone                   // calling Promise
//     .then(showOff)
//     .then(fulfilled => console.log(fulfilled))
//     .catch(error => console.log(error.message))
// }
// askMom();
//
//
//
//
//
//
//
//
//
